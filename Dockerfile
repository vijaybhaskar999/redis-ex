# Use an existing docker image as abase
FROM alpine

# Downoad and install a dependency 
RUN apk add --update redis
RUN apk add --update gcc


# tell the image what to do when it starts as a container 
CMD ["redis-server"]